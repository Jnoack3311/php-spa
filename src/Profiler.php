<?php
declare(strict_types=1);

namespace php_spa;

use JsonSerializable;
use php_spa\Configuration\Configuration;
use php_spa\Configuration\enums\ConfigurationEnum;

class Profiler implements JsonSerializable {
    public string $name;
    public float $start;
    public float $end;
    public float $runtime;
    public int $memory;
    public int $memoryPeak;
    public array $trace = [];
    public array $context = [];

    /** @var Profiler[] $levels  */
    protected static array $levels = [];

    protected static string $writerClass;

    public static function start(string $name, $context = []): void
    {
        static $initialized = false;
        if (!$initialized) {
            Configuration::initializeConfiguration();
            self::$writerClass = Configuration::$configuration[ConfigurationEnum::PROFILER_WRITER_CLASS->name];
            self::$writerClass::init();
            $initialized = true;
        }

        if (!Configuration::$configuration[ConfigurationEnum::PROFILER_ACTIVE->name]) {
            return;
        }

        $timer = new Profiler();
        $timer->name = $name;
        $timer->context = $context;
        $timer->start = \hrtime(true);
        $timer->memory = memory_get_usage();
        if (!empty(self::$levels)) {
            end(self::$levels)->trace[] = $timer;
        }
        self::$levels[] = $timer;
    }

    protected function end()
    {
        $this->end = hrtime(true);
        $this->runtime = $this->end - $this->start;
        $this->memory = memory_get_usage() - $this->memory;
        $this->memoryPeak = memory_get_peak_usage();

        return $this;
    }

    public static function stop(): void
    {
        if (!Configuration::$configuration[ConfigurationEnum::PROFILER_ACTIVE->name]) {
            return;
        }

        self::$writerClass::write(
            array_pop(self::$levels)->end()
        );
    }

    public static function endAllTimers(): void
    {
        while (!empty(self::$levels)) {
            self::stop();
        }
    }

    public function jsonSerialize(): array
    {
        return [
            'name' => $this->name,
            'runtime' => $this->runtime,
            'start' => $this->start,
            'end' => $this->end,
            'memory' => $this->memory,
            'memoryPeak' => $this->memoryPeak,
            'trace' => self::$levels,
            'context' => $this->context,
        ];
    }
}