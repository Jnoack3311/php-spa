<?php
declare(strict_types=1);

namespace php_spa\ResultGenerators\Html;

use \DirectoryIterator;
use php_spa\Configuration\Configuration;
use php_spa\Configuration\enums\ConfigurationEnum;
use php_spa\ResultGenerators\ResultGeneratorInterface;
use php_spa\Profiler;
use php_spa\ProfilerStatistics;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\VarDumper\VarDumper;

class ResultGenerator implements ResultGeneratorInterface {
    const SAVE_DIR = '/tmp/php-spa/results/';
    const JS_DIR = __DIR__ . '/Resources/js/';
    const CSS_DIR = __DIR__ . '/Resources/css/';
    const RESOURCES_DIR = __DIR__ . '/Resources/';


    public static function generate(?string $saveDir = null): void
    {
        Configuration::initializeConfiguration();
        $saveDir = self::getSaveDir($saveDir);
        $iterator = new DirectoryIterator(Configuration::$configuration[ConfigurationEnum::PROFILER_DIR->name]);
        if (!\file_exists(self::SAVE_DIR)) {
            \mkdir(self::SAVE_DIR, 0777, true);
        }

        foreach ($iterator as $file) {
            if (!$file->isDot()) {
                self::generateHtml($file, $saveDir);
            }
        }
    }

    private static function getSaveDir(?string $saveDir): string
    {
        $environmentSaveDir = getenv('PROFILER_RESULTS_DIR');
        if (!empty($saveDir)) {
            return $saveDir;
        } else if (!empty($environmentSaveDir)) {
            return $environmentSaveDir;
        }

        return self::SAVE_DIR;
    }

    private static function generateHtml(\SplFileInfo $file, string $saveDir): void
    {
        $chunks = explode(hex2bin('1c'), \file_get_contents($file->getPathname()));

        $callGraph = [];
        foreach ($chunks as $chunk) {
            $chunk = str_replace(hex2bin('1c'), '', $chunk);
            if (empty($chunk)) {
                continue;
            }

            $test =\unserialize($chunk);
            $callGraph = array_merge($callGraph, $test);
        }

        $file = \fopen($saveDir . $file->getFilename() . '.html', 'w+');
        $javascriptComponents = self::collectFileContents(self::JS_DIR . 'components/');
        $javascriptGlobals = file_get_contents(self::JS_DIR . 'helpers.js') . file_get_contents(self::JS_DIR . 'main.js');
        $css = self::collectFileContents(self::CSS_DIR);
        $htmlTemplate = \file_get_contents(self::RESOURCES_DIR . 'index.html');

        self::replaceTemplateVars('callGraph', json_encode($callGraph), $htmlTemplate);
        self::replaceTemplateVars('appCode', $javascriptComponents . $javascriptGlobals, $htmlTemplate);
        self::replaceTemplateVars('styles', $css, $htmlTemplate);

        \fwrite($file, $htmlTemplate);
        \fclose($file);
    }

    private static function replaceTemplateVars(string $variableName, string $content, string &$template): void
    {
        $template = str_replace("{{ $variableName }}", $content, $template);
    }

    private static function collectFileContents(string $directory): string
    {
        $directoryIterator = new RecursiveDirectoryIterator($directory);
        $iterator = new RecursiveIteratorIterator($directoryIterator);
        $content = '';

        foreach ($iterator as $file) {
            if (!$file->isDir()) {
                $content .= \file_get_contents($file->getPathname());
            }
        }

        return $content;
    }
}