Vue.component('line-graph-row', {
    props: [
        'data',
        'minMax',
    ],
    data: function () {
        return {
            color: getRandomColor(),
            width: 0,
        }
    },
    mounted: function () {
        this.width = this.$el.offsetWidth;

        new ResizeObserver(debounce(() => {
            this.width = this.$el.offsetWidth;
        }, 250)).observe(this.$el);
    },
    template: `
      <div class="row">
        <line-graph-row-entry v-for="entryData in data" :data="entryData" :min-max="minMax" :color="color" :width="width"></line-graph-row-entry>
      </div>
    `
})
