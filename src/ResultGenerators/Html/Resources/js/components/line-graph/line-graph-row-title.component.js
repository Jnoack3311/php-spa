Vue.component('line-graph-row-title', {
    props: [
        'data',
    ],
    data: function () {
        return {
            width: 0,
        };
    },
    computed: {
        charsAvailable: function () {
            return Math.floor(this.width / 7);
        },
        name: function () {
            let name = this.data.name;
            if (name.length > this.charsAvailable) {
                name = '...' + name.slice((name.length) - (this.charsAvailable - 3), name.length);
            }
            return name;
        }
    },
    mounted: function () {
        this.width = this.$el.offsetWidth;

        new ResizeObserver(debounce(() => {
            this.width = this.$el.offsetWidth;
        }, 250)).observe(this.$el);
    },
    template: `
      <span class="row-title">
        {{ name }}
      </span>
    `
})
