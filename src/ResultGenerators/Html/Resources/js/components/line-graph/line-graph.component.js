Vue.component('line-graph', {
    props: ['data'],
    computed: {
      zoom: function () {
        return this.$root.zoom * 100;
      },
      minMax: function () {
        let min = null;
        let max = null;

        this.data.forEach(timer => {
          if (min === null || max === null) {
            min = timer.start;
            max = timer.end;
            return;
          }

          if (timer.end > max) {
            max = timer.end;
          }

          if (timer.start < min) {
            min = timer.start;
          }
        });

        return { min, max }
      },
      leveledData: function () {
        let levels = this.data.map(timer => timer.name);
        levels = levels.filter((element, index, array) => array.indexOf(element) === index);
        const labels = [];
        const datasets = []

        levels.forEach(label => {
          const data = [];

          this.data.forEach(timer => {
            if (timer.name === label) {
              labels.push(label);
              data.push(timer);
            }
          });

          datasets.push(data);
        })

        return datasets.sort((a, b) => {
          let aMin = null;
          let bMin = null;
          a.forEach((element) => {
            if (aMin === null) {
              aMin = element.start;
            }

            if (element.start < aMin) {
              aMin = element.start;
            }
          })

          b.forEach((element) => {
            if (bMin === null) {
              bMin = element.start;
            }

            if (element.start < bMin) {
              bMin = element.start;
            }
          })

          if (aMin < bMin) {
            return -1;
          } else if (aMin > bMin) {
            return 1;
          } else {
            return 0;
          }
        });
      }
    },
    template: `
      <div class="graph">
        <div v-for="rowData in leveledData" class="row-wrapper" v-bind:style="{ width: zoom + '%' }">
          <line-graph-row-title :data="rowData[0]"></line-graph-row-title>
          <line-graph-row :data="rowData" :min-max="minMax"></line-graph-row>
        </div>
      </div>
    `
})
