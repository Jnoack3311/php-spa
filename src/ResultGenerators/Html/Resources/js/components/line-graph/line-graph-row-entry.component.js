Vue.component('line-graph-row-entry', {
    props: [
        'data',
        'minMax',
        'color',
        'zoom',
        'width',
    ],
    methods: {
      onEntryClick: function () {
          document.querySelectorAll('.row-entry.selected').forEach(element => {
              element.classList.remove('selected');
          })
          this.$el.classList.add('selected');
          this.$root.selectedData = this.data;
      }
    },
    computed: {
        totalDuration: function() {
            return this.minMax.max - this.minMax.min;
        },
        left: function () {
            const percentageStart = (this.data.start - this.minMax.min) / this.totalDuration;
            return Math.floor(this.width * percentageStart);
        },
        right: function () {
            const percentageEnd = (this.data.end - this.minMax.min) / this.totalDuration;
            return Math.ceil(this.width - this.width * percentageEnd);
        },
    },
    template: `
      <div class="row-entry"
           v-bind:style="{ left: left + 'px', right: right + 'px', backgroundColor: color }"
           v-on:click="onEntryClick()"
      >
      </div>
    `
})
