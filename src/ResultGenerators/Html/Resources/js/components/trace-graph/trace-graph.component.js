Vue.component('trace-graph', {
    props: ['data'],
    methods: {
        generateInheritance: function (topLevelNode, data) {
            let childNodes = [];
            data.forEach(entry => {
                if (entry.trace.length > 0 && entry.trace[0].name === topLevelNode.name) {
                    childNodes.push(entry);
                }
            })

            topLevelNode.childNodes = childNodes;
            return topLevelNode;
        },
        getColor: function () {
            return getRandomColor();
        }
    },
    computed: {
        leveledData: function () {
            let inheritanceData = [];

            this.data.forEach(entry => {
                if (!inheritanceData.some(inheritanceEntry => entry.name === inheritanceEntry.name)) {
                    entry.count = 1;
                    inheritanceData.push(entry);
                } else {
                    const inheritanceEntry = inheritanceData.find(inheritanceEntry => entry.name === inheritanceEntry.name);
                    inheritanceEntry.count++;
                }
            });

            inheritanceData = inheritanceData.map(entry => this.generateInheritance(entry, inheritanceData));
            let topLevelNodes = inheritanceData.filter(entry => entry.trace.length === 0);

            return topLevelNodes;
        },
    },
    template: `
      <div class="trace-graph">
        <div v-for="rowData in leveledData" class="trace-column" v-bind:style="{ backgroundColor: getColor() }">
            <div class="trace-entry">
                {{ rowData.name }}
            </div>
            
            <div class="trace-entry" v-for="traceEntry in rowData.childNodes">
                {{ traceEntry.name }} x {{ traceEntry.count }}
            </div>
        </div>
      </div>
    `
})
