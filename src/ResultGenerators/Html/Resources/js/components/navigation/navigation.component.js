Vue.component('navigation', {
    data: function () {
      return {
          currentGraph: this.$root.currentGraph,
      };
    },
    computed: {
      zoom: function () {
        return `${Math.round(this.$root.zoom * 100)}%`;
      },
    },
    methods: {
      resetZoom: function () {
        this.$root.zoom = 1;
      },
      changeGraph: function () {
          this.$root.currentGraph = this.currentGraph;
      }
    },
    template: `
      <div id="navigation">
        <div id="zoom-information">
          <span>
              {{ zoom }}
          </span>
          <button @click="resetZoom()">
            reset
          </button>
        </div>
        
        <div id="graph-switcher">
            <select v-model="currentGraph" @change="changeGraph()">
                <option value="line">timegraph</option>
                <option value="trace">tracegraph</option>
            </select>
        </div>
      </div>
    `
})
