Vue.component('additional-information', {
    computed: {
      data: function () {
        return this.$root.selectedData;
      },

      display: function () {
        if (!!this.$root.selectedData.name) {
          return 'block';
        }

        return 'none';
      }
    },
    methods: {
      formatMemoryUsage: function (memory) {
        const units = ['b','kb','mb','gb','tb','pb'];
        let i = 0;

        // stackoverflow ftw x)
        return Math.round(memory/Math.pow(1024,(i=Math.floor(Math.log(memory)/ Math.log(1024))))) + ' ' + units[i];
      },
      formatNanosecondDuration: function (duration) {
        const units = ['ns','us','ms','s','m','h'];
        let i = 0;

        return Math.round(duration/Math.pow(1000,(i=Math.floor(Math.log(duration)/ Math.log(1000))))) + ' ' + units[i];
      },
      close: function () {
        this.$root.selectedData = {};
        document.querySelectorAll('.row-entry.selected').forEach(element => {
          element.classList.remove('selected');
        })
      },
    },
    template: `
      <div class="additional-information" v-bind:style="{ display: display }">
        <div class="close" v-on:click="close()">
            X
        </div>
        <p class="title">
           {{ data.name }}
        </p>
        <div class="data">
          <div class="data-entry">
            <div>start:</div>
            <div>{{ data.start }}</div>
          </div>
          <div class="data-entry">
            <div>end:</div>
            <div>{{ data.end }}</div>
          </div>
          <div class="data-entry">
            <div>runtime:</div>
            <div>{{ formatNanosecondDuration(data.runtime) }}</div>
          </div>
          <div class="data-entry">
            <div>memory:</div>
            <div>{{ formatMemoryUsage(data.memory) }}</div>
          </div>
          <div class="data-entry">
            <div>memory peak:</div>
            <div>{{ formatMemoryUsage(data.memoryPeak )}}</div>
          </div>
          <p>
          context:
            <pre class="context">
                {{ data.context }}
            </pre>      
          </p>
        </div>
      </div>
    `
})
