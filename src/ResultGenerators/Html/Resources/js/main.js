let app = new Vue({
    el: '#app',
    data: {
        callGraph: {},
        selectedData: {},
        currentGraph: 'line',
        zoom: 1,
    },
    created: function () {
        console.log(callGraph);
        this.callGraph = callGraph;
        window.addEventListener('wheel', (event) => {
            event.preventDefault();
            this.zoom += event.deltaY * -0.01;
            this.zoom = Math.min(Math.max(1, this.zoom), 100);
        });
    },
})
