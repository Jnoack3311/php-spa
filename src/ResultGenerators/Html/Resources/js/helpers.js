
function debounce(func, wait, immediate) {
    let timeout;

    return function () {
        let context = this,
            args = arguments,
            callNow = immediate && !timeout;

        let later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };

        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

const goldenAngle = 180 * (3 - Math.sqrt(5))

function getRandomColor() {
    return `hsl(${Math.floor(Math.random() * 360) * goldenAngle + 60}, 100%, 75%)`;
}
