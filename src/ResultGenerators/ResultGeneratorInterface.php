<?php
declare(strict_types=1);

namespace php_spa\ResultGenerators;

interface ResultGeneratorInterface {
    public static function generate(?string $saveDir): void;
}