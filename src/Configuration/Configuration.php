<?php
declare(strict_types=1);

namespace php_spa\Configuration;

use php_spa\Configuration\attributes\ConfigurationTypeAttribute;
use php_spa\Configuration\attributes\DefaultConfigurationValueAttribute;
use php_spa\Configuration\enums\ConfigurationEnum;
use php_spa\Configuration\enums\ConfigurationTypeEnum;

class Configuration {
    public static array $configuration = [];

    public static function initializeConfiguration(): void
    {
        foreach (ConfigurationEnum::cases() as $configName) {
            if (!empty(self::$configuration[$configName->name])) {
                continue;
            }

            /** @var ?ConfigurationTypeAttribute $reflectionAttribute */
            $reflectionAttribute = self::getConfigurationAttribute($configName, ConfigurationTypeAttribute::class);
            if (empty($reflectionAttribute)) {
                continue;
            }

            $configurationValue = \getenv($configName->name);
            if (!empty($configurationValue)) {
                self::$configuration[$configName->name] = self::parseConfigurationType($configurationValue, $reflectionAttribute->type);
            } else {
                /** @var ?DefaultConfigurationValueAttribute $defaultConfig */
                $defaultConfig = self::getConfigurationAttribute($configName, DefaultConfigurationValueAttribute::class);
                self::$configuration[$configName->name] = $defaultConfig->defaultValue;
            }
        }
    }

    private static function parseConfigurationType(string $value, ConfigurationTypeEnum $configurationType): mixed
    {
        return match ($configurationType->name) {
            ConfigurationTypeEnum::TYPE_STRING->name => $value,
            ConfigurationTypeEnum::TYPE_BOOL->name   => boolval($value),
            ConfigurationTypeEnum::TYPE_INT->name    => intval($value),
        };
    }

    private static function getConfigurationAttribute(ConfigurationEnum $configurationEnum, string $attributeClass): ?object
    {
        static $configurationReflection = new \ReflectionClass(ConfigurationEnum::class);
        $reflectionConstant = $configurationReflection->getReflectionConstant($configurationEnum->name);
        if (!$reflectionConstant) {
            return null;
        }
        $reflectionAttributes = $reflectionConstant->getAttributes($attributeClass);
        if (empty($reflectionAttributes)) {
            return null;
        }

        return $reflectionAttributes[0]->newInstance();
    }

}