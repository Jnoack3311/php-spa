<?php

namespace php_spa\Configuration\attributes;

use php_spa\Configuration\enums\ConfigurationTypeEnum;

#[\Attribute(\Attribute::TARGET_CLASS_CONSTANT)]
class ConfigurationTypeAttribute
{
    public function __construct(public readonly ConfigurationTypeEnum $type)
    {
    }
}