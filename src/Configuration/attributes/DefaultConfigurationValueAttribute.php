<?php

namespace php_spa\Configuration\attributes;

#[\Attribute(\Attribute::TARGET_CLASS_CONSTANT)]
class DefaultConfigurationValueAttribute
{
    public function __construct(public readonly mixed $defaultValue)
    {
    }
}