<?php

namespace php_spa\Configuration\enums;

enum ConfigurationTypeEnum
{
    case TYPE_BOOL;
    case TYPE_STRING;
    case TYPE_INT;
}
