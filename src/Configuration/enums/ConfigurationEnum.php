<?php

namespace php_spa\Configuration\enums;

use php_spa\Configuration\attributes\ConfigurationTypeAttribute;
use php_spa\Configuration\attributes\DefaultConfigurationValueAttribute;

enum ConfigurationEnum
{

    #[ConfigurationTypeAttribute(ConfigurationTypeEnum::TYPE_BOOL)]
    #[DefaultConfigurationValueAttribute(false)]
    case PROFILER_ACTIVE;

    #[ConfigurationTypeAttribute(ConfigurationTypeEnum::TYPE_STRING)]
    #[DefaultConfigurationValueAttribute('/tmp/php-spa/profiling/')]
    case PROFILER_DIR;

    #[ConfigurationTypeAttribute(ConfigurationTypeEnum::TYPE_INT)]
    #[DefaultConfigurationValueAttribute(2000)]
    case PROFILER_STREAMING_BATCH_SIZE;

    #[ConfigurationTypeAttribute(ConfigurationTypeEnum::TYPE_STRING)]
    #[DefaultConfigurationValueAttribute('\php_spa\Writer\StreamingWriter')]
    case PROFILER_WRITER_CLASS;
}
