<?php

namespace php_spa\Writer;

use php_spa\Configuration\Configuration;
use php_spa\Configuration\enums\ConfigurationEnum;
use php_spa\Profiler;

class StreamingWriter implements WriterInterface
{

    protected static $callGraph = [];

    protected static int $streamingCounter = 0;

    protected static mixed $fileHandle;

    public static function init(): void
    {
        $fileName = \sprintf('profiler%s.txt', \microtime(true));
        $fileDir = Configuration::$configuration[ConfigurationEnum::PROFILER_DIR->name];

        if (!\file_exists($fileDir)) {
            \mkdir($fileDir, 0777, true);
        }

        self::$fileHandle = fopen($fileDir . $fileName, 'w');
        register_shutdown_function(fn() => self::shutdown());
    }

    public static function write(Profiler $timer): void
    {
        self::$callGraph[] = $timer;

        self::$streamingCounter++;
        if (self::$streamingCounter > Configuration::$configuration[ConfigurationEnum::PROFILER_STREAMING_BATCH_SIZE->name]) {
            self::flush();
            self::$streamingCounter = 0;
        }
    }

    static private function shutdown()
    {
        self::flush();
        fclose(self::$fileHandle);
    }

    private static function flush()
    {

        fwrite(self::$fileHandle, \serialize(self::$callGraph) . hex2bin('1c'));
        self::$callGraph = [];
    }
}