<?php

namespace php_spa\Writer;

use php_spa\Configuration\Configuration;
use php_spa\Configuration\enums\ConfigurationEnum;
use php_spa\Profiler;

class NullWriter implements WriterInterface
{
    public static function init(): void
    {}

    public static function write(Profiler $timer): void
    {}
}