<?php

namespace php_spa\Writer;

use php_spa\Profiler;

interface WriterInterface
{
    static function init();
    static function write(Profiler $timer);
}