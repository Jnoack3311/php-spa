<?php
use php_spa\Profiler;
use php_spa\ResultGenerators\Html\ResultGenerator;
use php_spa\ProfilerStatistics;

require_once __DIR__ . "/vendor/autoload.php";

exec('rm -r /tmp/php-spa/');
exec('php demo/test-script.php');

ResultGenerator::generate();
