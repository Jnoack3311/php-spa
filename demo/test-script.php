<?php

use php_spa\Profiler;

require_once __DIR__ . '/../vendor/autoload.php';

putenv('PROFILER_ACTIVE=true');
Profiler::start('im a parent timer');
for ($index = 0; $index < 100000; $index++) {
    Profiler::start('im a child timer', ['index' => $index]);
    Profiler::stop();
}
Profiler::stop();

