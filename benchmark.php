<?php

require_once __DIR__ . "/vendor/autoload.php";

use php_spa\Benchmark;

$results = [
    'memory' => [
        Benchmark\Memory\DeepBenchmark::class => Benchmark\Memory\DeepBenchmark::benchmark(),
        Benchmark\Memory\BulkEntriesBenchmark::class => Benchmark\Memory\BulkEntriesBenchmark::benchmark(),
    ],
    'cpu' => [
        Benchmark\CPU\ImpactBenchmark::class => Benchmark\CPU\ImpactBenchmark::benchmark(),
        Benchmark\CPU\ImpactDeactivatedBenchmark::class => Benchmark\CPU\ImpactDeactivatedBenchmark::benchmark(),
    ],
];

var_dump($results);