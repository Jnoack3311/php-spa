<?php

namespace php_spa\Benchmark\CPU;

use php_spa\Configuration\Configuration;
use php_spa\Configuration\enums\ConfigurationEnum;
use php_spa\Profiler;
use php_spa\ProfilerStatistics;
use PhpBench\Attributes as Bench;

class ActivatedImpactBenchmark
{
    public function setUp()
    {
        gc_collect_cycles();
    }

    #[Bench\Revs(2)]
    #[Bench\BeforeMethods('setUp')]
    public function benchProfilerRemoved(): void
    {
        for ($index = 0; $index < 100000; $index++) {
        }
    }

    #[Bench\Revs(2)]
    #[Bench\BeforeMethods('setUp')]
    public function benchProfilerDisabled(): void
    {
        for ($index = 0; $index < 100000; $index++) {
            Profiler::start('test');
            Profiler::stop();
        }
    }

    #[Bench\Revs(2)]
    #[Bench\BeforeMethods('setUp')]
    public function benchProfilerEnabled(): void
    {
        Configuration::initializeConfiguration();
        Configuration::$configuration[ConfigurationEnum::PROFILER_ACTIVE->name] = true;
        for ($index = 0; $index < 100000; $index++) {
            Profiler::start('test');
            Profiler::stop();
        }
    }
}