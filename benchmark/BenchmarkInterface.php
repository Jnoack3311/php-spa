<?php

namespace php_spa\Benchmark;

interface BenchmarkInterface
{
    public static function benchmark(): float;
}