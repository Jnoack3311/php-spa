# php-spa
### Simple performance analyses

## Usage

The Profiler needs the `PROFILER_ACTIVE` environment variable to be set to `true` or `1` to run.
Then you need to add following function calls to the code you wanna analyse:

### `Profiler::start($timerName, $context = []);`

Starts the timer with the given name and records timings and ram usage. 
You can start multiple timers within each other for hierarchy to be shown in the results.
Context can be added to the timer for additional data in the HTML result to show at wich data yout code may be slow.

### `Profiler::stop();`

Stops the last started timer and puts its in the `ProfilerStatistics`.

### `ProfilerStatistics::save();`

Saves all stopped timers to `/tmp/php-spa/profiling/`.
You can also change the directory where the timers are saved via  the `PROFILER_DIR` environment variable.

### `ResultGenerator::generate($saveDir);`

Generates HTML results of all saved timers contained in `PROFILER_DIR` and saves the results by default to `/tmp/php-spa/results/`. You can also change the directory where
the results are saved via the `$saveDir` parameter or the `PROFILER_RESULTS_DIR` environment variable. If both, the `PROFILER_RESULTS_DIR` environment variable and the `$saveDir` parameter is set, the `$saveDir` parameter is used.

### Example

    use php_spa\Profiler;
    use php_spa\Generators\Html\ResultGenerator;
    use php_spa\ProfilerStatistics;
    
    require_once __DIR__ . "/vendor/autoload.php";

    putenv('PROFILER_ACTIVE=true');
    Profiler::start('doing something');
    for ($index = 0;$index < 1000; $index++) {
        usleep(100);
    }
    Profiler::stop();
    
    Profiler::start('im a parent timer');
    for ($index = 0;$index < 100; $index++) {
        Profiler::start('im a child timer', ['index' => $index]);
        usleep(100);
        Profiler::stop();
    }
    Profiler::stop();
    
    ProfilerStatistics::save();
    ResultGenerator::generate();
